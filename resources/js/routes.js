window.Vue = require('vue');
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import ExampleComponent from './components/ExampleComponent';
import DashboardComponent from './components/DashboardComponent';
import CollectionsComponent from './components/CollectionsComponent';

export default new VueRouter({
    routes: [
        {
            path: '/example',
            component: ExampleComponent,
            meta: {
                breadcrumbs: [
                    { name: 'Home', link: '/home' },
                    { name: 'Example' }
                ],
                title: 'Example Component Test'
            }
        },
        {
            path: '/home',
            component: DashboardComponent,
            meta: {
                breadcrumbs: [
                    { name: 'Home' }
                ],
                title: 'Dashboard'
            }
        },
        {
            path: '/collections',
            component: CollectionsComponent,
            meta: {
                breadcrumbs: [
                    { name: 'Home', link: '/home' },
                    { name: 'Collection' }
                ],
                title: 'Collection'
            }
        },
    ],
    mode: 'history'
})