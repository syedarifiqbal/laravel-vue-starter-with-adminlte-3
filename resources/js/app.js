
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.VueRouter = require('vue-router');

Vue.use(VueRouter);

import router from './routes.js';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('preloader', require('./components/PreloaderComponent.vue'));
Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('breadcrumbs', require('./components/Breadcrumbs.vue'));
Vue.component('page-title', require('./components/PageTitleComponent.vue'));
Vue.component('sidebar', require('./components/SidebarComponent.vue'));
Vue.component('collections', require('./components/CollectionsComponent.vue'));

// import { TableComponent, TableColumn } from 'vue-table-component';
// Vue.component('table-component', TableComponent);
// Vue.component('table-column', TableColumn);
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.component('laravel-datatable', require('./components/LaravelDatatableComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
